#ifndef HASHSET
#define HASHSET

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linkedlist.h"

typedef struct {
	List** table;
	unsigned long size;
	char load_factor; // 0 - 100
}HashSet;

// Generate a prehash for an item with a given size
unsigned long prehash(void* item, unsigned int item_size){
	
	unsigned long h = 5381;
    int x;

    for(int i = 0; i < item_size; i++){
        x = *(((int*)item++));
        h = ((h << 5) + h) + x;
    }

    return h;

}

// Hash an unsigned long into an index that fits into a hash set
unsigned long hash(unsigned long prehash, unsigned long buckets){

}

// Initialize an empty hash set with a given size
void initHashSet(HashSet* hashset_pointer, unsigned int size){
	hashset_pointer->size = size;
	hashset_pointer->load_factor = 0;
    List *list[size];
    hashset_pointer->table = list;

    for (int i = 0; i < size; i++){
        hashset_pointer->table[i] = (List*)malloc(sizeof(List));
        initList(hashset_pointer->table[i]);
		hashset_pointer->table[i]->head = NULL;
    }

}

// Insert item in the set. Return true if the item was inserted, false if it wasn't (i.e. it was already in the set)
// Recalculate the load factor after each successful insert (round to nearest whole number).
// If the load factor exceeds 70 after insert, resize the table to hold twice the number of buckets.
bool insertItem(HashSet* hashset_pointer, void* item, unsigned int item_size){

	unsigned long prehash = prehash(item, item_size);
	unsigned long hash = hash(prehash, item_size);
	insertAtHead(hashset_pointer->table[hash], item);

	if (hashset_pointer->table[hash] == item){
		hashset_pointer->load_factor++;
	}
	else{
		return false;
	}

	if (hashset_pointer->load_factor > 70){
		resizeTable(hashset_pointer, hashset_pointer->size*2);
		hashset_pointer->load_factor = 0;
	}

	return true;

}

// Remove an item from the set. Return true if it was removed, false if it wasn't (i.e. it wasn't in the set to begin with)
bool removeItem(HashSet* hashset_pointer, void* item, unsigned int item_size){
	unsigned long prehash = prehash(item, item_size);
	unsigned long hash = hash(prehash, item_size);
	if (findItem(hashset_pointer, item, item_size) == true){
		hashset_pointer->table[hash] = NULL;
		hashset_pointer->load_factor--;
		return true;
	}
	return false;
}

// Return true if the item exists in the set, false otherwise
bool findItem(HashSet* hashset_pointer, void* item, unsigned int item_size){

	unsigned long prehash = prehash(item, item_size);
	unsigned long hash = hash(prehash, item_size);
	int j = 0;
	void* curr = hashset_pointer->table[i]->head;

	while (curr != hashset_pointer->table[hash]->tail){
		if (curr == item){
			return true;
		}
		else{
			curr = hashset_pointer->table[i]->head->next;
		}
	}
	return false;
}

// Resize the underlying table to the given size. Recalculate the load factor after resize
void resizeTable(HashSet* hashset_pointer, unsigned int new_size){
	hashset_pointer->size = new_size;
	hashset_pointer->load_factor = 0;
}

// Print Table
void printHashSet(HashSet* hashset_pointer){
	for (int i = 0; i < hashset_pointer->size; i++){
		printList(hashset_pointer->table[i]);
	}
}

#endif